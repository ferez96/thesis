package gitlab.ferez96.thesis;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

/**
 * This class statically load the configuration file
 * @author MinhDT
 *
 */
public class Configs {
    private static final Logger log = Logger.getLogger(Configs.class);
    private static final String DEFAULT_CONFIG_FILE = "app.yaml";
    private static boolean isLoaded = false;
    public static String apikey;
    public static String apkDir;
    public static String apkTempDir;
    public static String androidJars;
    public static ArrayList<String> apkSha256;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static boolean loadYaml(URL yamlUrl) throws JsonParseException, JsonMappingException, IOException {
        log.info("load config from " + yamlUrl);
        Map configs = new ObjectMapper(new YAMLFactory()).readValue(yamlUrl.openStream(), Map.class);
        try {
            apikey = (String) configs.getOrDefault("ApiKey", apikey);
            apkDir = (String) configs.getOrDefault("ApkDir", apkDir);
            apkTempDir = (String) configs.getOrDefault("ApkTempDir", apkTempDir);
            apkSha256 = (ArrayList) configs.getOrDefault("ApkSHA256", apkSha256);
            androidJars = (String) configs.getOrDefault("AndroidJars", androidJars);
            isLoaded = true;
            return true;
        } catch (Throwable e) {
            log.error("Load YAML config values fail", e);
            isLoaded = false;
            return false;
        }
    }

    public static boolean load() {
        if (isLoaded) return true;
        boolean loaded = false;
        try {
            URL yamlUrl = new File(DEFAULT_CONFIG_FILE).exists() ? new URL(DEFAULT_CONFIG_FILE) : null;
            yamlUrl = yamlUrl == null ? Configs.class.getClassLoader().getResource(DEFAULT_CONFIG_FILE) : yamlUrl;
            yamlUrl = yamlUrl == null ? Configs.class.getResource(DEFAULT_CONFIG_FILE) : yamlUrl;

            if (yamlUrl != null) {
                loaded = loadYaml(yamlUrl);
            } else {
                log.error("could not find app.yaml config file");
            }
        } catch (Exception e) {
            log.warn("Load config fail", e);
        }
        return loaded;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty("line.separator");

        result.append(this.getClass().getName());
        result.append(" Object {");
        result.append(newLine);

        // determine fields declared in this class only (no fields of superclass)
        Field[] fields = this.getClass().getDeclaredFields();

        // print field names paired with their values
        for (Field field : fields) {
            result.append("  ");
            try {
                result.append(field.getName());
                result.append(": ");
                // requires access to private field:
                result.append(field.get(this));
            } catch (IllegalAccessException ex) {
                System.out.println(ex);
            }
            result.append(newLine);
        }
        result.append("}");

        return result.toString();
    }
}
