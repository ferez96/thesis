package gitlab.ferez96.thesis;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import soot.Body;
import soot.Context;
import soot.Scene;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.InvokeStmt;
import soot.jimple.infoflow.android.SetupApplication;
import soot.jimple.toolkits.callgraph.CallGraph;

public class Main {
    private static final Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        Configs.load();
        System.out.println(new Configs());

        Path apksPath = Paths.get(Configs.apkDir);
        Optional<Path> apk = null;
        try {
            apk = Files.walk(apksPath).filter(p -> !Files.isDirectory(p) && p.toString().endsWith("apk")).findAny();
            if (apk.isPresent()) {
                System.out.println("Process APK: " + apk.get());
                SetupApplication app = new SetupApplication(Configs.androidJars, apk.get().toString());
                app.constructCallgraph();
                CallGraph callGraph = Scene.v().getCallGraph();
                visitCallGraph(callGraph);
//                Graph<String, DefaultEdge> G = callGraph2Num(Scene.v().getCallGraph());
//                G.edgeSet().forEach(System.out::println);
            } else {
                throw new IOException();
            }
        } catch (IOException e) {
            System.out.println("No APK found");
            e.printStackTrace(System.err);
        }
        System.out.println("Program Exit!");
    }

    static void visitCallGraph(CallGraph graph) {
        System.out.println("Call graph Size: " + graph.size() + " edges");
        Set<String> vertices = new HashSet<>();
        graph.forEach(e -> {
            Context srcContext = e.getSrc().context();
            SootMethod srcMethod = e.getSrc().method();
            Context tgtContext = e.getTgt().context();
            SootMethod tgtMethod = e.getTgt().method();
            System.out.println(srcContext);
            System.out.println(srcMethod);
            System.out.println(tgtContext);
            System.out.println(tgtMethod);
            vertices.add(e.getSrc().toString());
            vertices.add(e.getTgt().toString());
        });
        System.out.println("Number of vertices: " + vertices.size());
    }

    static Graph<String, DefaultEdge> callGraph2Num(CallGraph graph) {
        Graph<String, DefaultEdge> G = new SimpleGraph<>(DefaultEdge.class);
        graph.forEach(e -> {
            log.info("Add edge: " + e.getSrc() + "\t->\t" + e.getTgt());
            G.addVertex(e.getSrc().getClass().toString());
            G.addVertex(e.getTgt().getClass().toString());
            G.addEdge(e.getSrc().getClass().toString(), e.getTgt().getClass().toString());
        });
        return G;
    }

    @Deprecated
    static void visitBody(Body body, PrintStream out) {
        body.getAllUnitBoxes().forEach(box -> {
            Unit unit = box.getUnit();
            out.printf("%10s - %s\n", unit.getClass().getSimpleName(), unit);
            if (unit instanceof InvokeStmt) {
                out.println("\t- " + ((InvokeStmt) unit).getInvokeExprBox());
            }
        });
    }
}                                              
