package gitlab.ferez96.thesis;

import soot.Body;
import soot.Unit;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.graph.UnitGraph;

public class ICFG extends UnitGraph implements DirectedGraph<Unit>{

    protected ICFG(Body body) {
        super(body);
    }

}
