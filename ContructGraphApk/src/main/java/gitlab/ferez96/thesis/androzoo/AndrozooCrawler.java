package gitlab.ferez96.thesis.androzoo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import gitlab.ferez96.thesis.Configs;

public class AndrozooCrawler {

    private static final Logger log = Logger.getLogger(AndrozooCrawler.class);
    private static final String AndrozooRequest = "https://androzoo.uni.lu/api/download?apikey=${APIKEY}&sha256=${SHA256}";

    public AndrozooCrawler() {
        Configs.load();
    }

    public int cralw(String sha256) {
        int attemptCnt = 1;
        if (attemptCnt <= 3) {
            log.info("Crawling hash key:" + sha256 + (attemptCnt > 1 ? "Attempt:" + attemptCnt : ""));
            String targetDir = Configs.apkTempDir;
            Path targetPath;
            String apiKey = Configs.apikey;

            // check
            targetPath = Paths.get(targetDir);
            boolean dirExistCheck = Files.exists(targetPath);
            if (!dirExistCheck) {
                try {
                    Files.createDirectories(targetPath);
                } catch (IOException e) {
                }
                dirExistCheck = Files.exists(targetPath);
            }
            if (!dirExistCheck) {
                targetDir = System.getProperty("java.io.tmpdir");
                log.warn("ApkTempDir: " + Configs.apkTempDir + " is not available, load default tmp folder: "
                        + targetDir);
            }
            boolean dirWriteableCheck = Files.isWritable(targetPath);
            if (dirWriteableCheck) {
                targetPath = Paths.get(targetDir, sha256 + ".apk");
                boolean fileExistCheck = Files.exists(targetPath);
                if (fileExistCheck) {
                    try {
                        Files.delete(targetPath);
                        log.info("Old apk file is deleted");
                    } catch (IOException e) {
                        log.error("Deleting file causes error", e);
                        if (Files.exists(targetPath)) return -1;
                    }
                }
            } else {
                log.error("Do not have write permission");
                return -1;
            }

            // crawl
            String url = AndrozooRequest.replace("${APIKEY}", apiKey).replace("${SHA256}", sha256);
            HttpGet request = new HttpGet(url);
            HttpClient client = HttpClientBuilder.create().build();
            try {
                HttpResponse resp = client.execute(request);
                int statusCode = resp.getStatusLine().getStatusCode();
                switch (statusCode) {
                case 200:
                    InputStream stream = resp.getEntity().getContent();
                    OutputStream out = new FileOutputStream(targetPath.toFile());
                    long size = resp.getEntity().getContentLength();
                    while (size-- > 0) {
                        out.write(stream.read());
                    }
                    stream.close();
                    out.close();
                    break;
                default:
                    log.error("Request fail! status code: " + statusCode);
                }
            } catch (IOException e) {
                attemptCnt++;
            }
        }

        return 0;
    }

}
