package gitlab.ferez96.thesis.androzoo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

public class DatasetInitor {

    private static final Logger log = Logger.getLogger(DatasetInitor.class);
    private static final String LabelUrl = "D:/Datasets/apks/androzoo/latest_20180901.csv";
    private static final int NumberOfApks = 500000;
    private static Random rand = new Random(1007);

    public List<String> get(int n) {
        List<String> ans = new ArrayList<>();
        Set<Integer> indexes = new TreeSet<>();
        while (indexes.size() < n) {
            indexes.add(rand.nextInt(NumberOfApks - 17));
        }
        try {
            Scanner scanner = new Scanner(new FileInputStream(LabelUrl));
            Iterator<Integer> indexIter = indexes.iterator();
            int currentLine = 0;
            int currentIndex = indexIter.next();
            String line;
            scanner.nextLine();// ignored first line
            while (indexIter.hasNext() && currentLine < NumberOfApks) {
                line = scanner.nextLine();
                if (currentLine == currentIndex) {
                    String x = line.split(",")[0];
                    ans.add(x);
                    log.info("Dataset sellected apk id="+currentIndex);
                    currentIndex = indexIter.next();
                }
                currentLine++;
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            log.error("Labels file not found, please check the url:" + LabelUrl, e);
        }
        return ans;
    }

    public static void main(String[] args) {
        List<String> list = new DatasetInitor().get(20);
        for (String x : list) {
            System.out.println("\t-\t"+x);
        }
    }
}
