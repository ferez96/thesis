package gitlab.ferez96.thesis;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import gitlab.ferez96.thesis.androzoo.AndrozooCrawler;

public class CrawlingTest {

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    @Disabled("Raw test")
    @Test
    void testRawCrawling() {
        Configs.load();
        String sha256 = "015D558B8C89A72D721151CE2F11E153D58F9EBAAA78C7312D9343B75C76FC95";
        String AndrozooRequest = "https://androzoo.uni.lu/api/download?apikey=${APIKEY}&sha256=${SHA256}";
        String url = AndrozooRequest.replace("${APIKEY}", Configs.apikey).replace("${SHA256}", sha256);
        System.out.println("Connecting to: " + url + " ...");
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(url);
            HttpResponse resp = client.execute(request);
            int statusCode = resp.getStatusLine().getStatusCode();
            assertEquals(200, statusCode, "Http Response Status Code");
            System.out.println("Connection success!");
            long size = resp.getEntity().getContentLength();
            System.out.println("File size:" + size);
            InputStream stream = resp.getEntity().getContent();
            ;
            // Check sha256
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                DigestInputStream dStream = new DigestInputStream(stream, digest);
                long startTimestamp = System.currentTimeMillis();
                byte[] data = new byte[1024];
                while (size > 0) {
                    size -= dStream.read(data);
                }
                byte[] encodedHash = dStream.getMessageDigest().digest();
                long finnishTimestamp = System.currentTimeMillis();
                System.out.println("Digest time: " + (0.0 + finnishTimestamp - startTimestamp) / 1000 + "s");
                String hashCode = bytesToHex(encodedHash).toUpperCase();

                assertEquals(sha256, hashCode, "SHA256 check");
            } catch (NoSuchAlgorithmException e) {
            }

        } catch (IOException e) {
        }
    }

    @Test
    void testCrawler() throws NoSuchAlgorithmException, IOException {
        String sha256 = "015D558B8C89A72D721151CE2F11E153D58F9EBAAA78C7312D9343B75C76FC95";
        long startTimestamp = System.currentTimeMillis();
        int result = new AndrozooCrawler().cralw(sha256);
        long finnishTimestamp = System.currentTimeMillis();
        System.out.println("Download time: " + (0.0 + finnishTimestamp - startTimestamp) / 1000 + "s");
        assertEquals(0, result);
        Path expectedPath = Paths.get(Configs.apkTempDir, sha256 + ".apk");
        assertTrue(Files.exists(expectedPath));
        FileInputStream stream = new FileInputStream(expectedPath.toFile());
        DigestInputStream dStream = new DigestInputStream(stream, MessageDigest.getInstance("SHA-256"));
        long size = Files.size(expectedPath);
        byte[] data = new byte[1024];
        while (size > 0) {
            size -= dStream.read(data);
        }
        byte[] encodedHash = dStream.getMessageDigest().digest();
        String hashCode = bytesToHex(encodedHash).toUpperCase();
        assertEquals(sha256, hashCode, "SHA256 check");
        dStream.close();
    }
}
