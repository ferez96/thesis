package gitlab.ferez96.thesis;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.OS;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class TestFlowDroid {

    @Test
    @DisabledOnOs(OS.WINDOWS)
    void testReadYaml() {
        assertNotNull(TestFlowDroid.class.getResource("app.yaml"));
        Path ymlFile = Paths.get(TestFlowDroid.class.getResource("app.yaml").getPath());
        assertTrue(Files.exists(ymlFile));
        assertTrue(Files.isReadable(ymlFile));
        YAMLFactory factory = new YAMLFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        @SuppressWarnings("rawtypes")
        Map object = assertDoesNotThrow(
                () -> mapper.readValue(TestFlowDroid.class.getResourceAsStream("app.yaml"), Map.class));
        assertNotNull(object.get("ApkTempDir"));
        assertNotNull(object.get("ApiKey"));
    }

    @Test
    void testLoadConfigs() {
        assertTrue(Configs.load());
        assertNotNull(Configs.apikey, "apikey");
        assertNotNull(Configs.apkDir, "apkDir");
    }
}
